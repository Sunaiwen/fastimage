/**
 * checking the width and height of a img from its head info very fast
 * @param {String} image url
 * @param {Function} invoke when the width and height is checked
 * @param {Function} invoke when the onload event of the img is fired (optional)
 * @param {Function} invoke when the onerror event of the img is fired (optional)
 */

;(function(){
    var tickArr = [];
    var timerId;
    var stopTick = function(){
        clearTimeout(timerId);
        timerId = null;
    };

    var tick = function(){
        for(var i=0; i<tickArr.length; i++) {
            if(tickArr[i].checked === true) {
                tickArr.splice(i, 1);
                tickArr.length === 0 ? stopTick() : i-=1;
            } else {
                tickArr[i]();
            }
        }
    };
    var fastImage = function(imgUrl, infoReady, imgLoaded, imgError){
        if(infoReady === undefined) {
            console.error('infoReady missing');
            return;
        }
        var img = new Image();
        img.src = imgUrl;
        var oldWidth = img.width;
        var oldHeight = img.height;
        var check = function(){
            var newWidth = img.width;
            var newHeight = img.height;
            if(newWidth !== oldWidth && newHeight !==oldHeight) {
                check.checked = true;
                infoReady(newWidth, newHeight);
            }
        };

//        if img is cached before
        if(img.complete === true) {
            infoReady(img.width, img.height);
            imgLoaded && imgLoaded(img.width, img.height);
            !check.checked && check();
            img.onload = img.onerror = null;
        }

        img.onload = function(){
            imgLoaded && imgLoaded(img.width, img.height);
            !check.checked && check();
            img.onload = img.onerror = null;
        };

        img.onerror = function(){
            check.checked = true;
            imgError && imgError();
            img.onload = img.onerror = null;
        };

        if(timerId == undefined) {
            tickArr.push(check);
//            only allow one timer in process
            timerId = setInterval(tick, 50);
        }
    };
    window.OE = window.OE || {};
    window.OE.fastImage = fastImage;
    if(typeof define === 'function') {
        define(function(){
            return fastImage;
        });
    }
})();